package com.test.group;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import java.util.jar.Manifest;
import java.util.Map;
import java.io.IOException;
import java.util.jar.Attributes;
import java.net.URL;
import java.net.URLClassLoader;


// Plain old Java Object it does not extend as class or implements
// an interface

// The class registers its methods for the HTTP GET request using the @GET annotation.
// Using the @Produces annotation, it defines that it can deliver several MIME types,
// text, XML and HTML.

// The browser requests per default the HTML MIME type.

//Sets the path to base URL + /hello
@Path("/hello")
public class Hello {

  // This method is called if TEXT_PLAIN is request
  @GET
  @Produces(MediaType.TEXT_PLAIN)
  public String sayPlainTextHello() {
    return "Hello Jersey1";
  }

  // This method is called if XML is request
  @GET
  @Produces(MediaType.TEXT_XML)
  public String sayXMLHello() {
    return "<?xml version=\"1.0\"?>" + "<hello> Hello Jersey1" + "</hello>";
  }

  // This method is called if HTML is request
  @GET
  @Produces(MediaType.TEXT_HTML)
  public String sayHtmlHello() {
	  String extra = "";
	URLClassLoader cl = (URLClassLoader) getClass().getClassLoader();
	try {
		URL url = cl.findResource("META-INF/MANIFEST.MF");
		Manifest manifest = new Manifest(url.openStream());
		for(Map.Entry<String,Attributes> entry : manifest.getEntries().entrySet()){
			extra += entry.getKey() +": " + entry.getValue() + "<br>";
		}
	} catch (IOException E) {
	// handle
	}
    return "<html> " + "<title>" + "Hello Jersey1" + "</title>"
        + "<body><h1>" + "Hello Jersey1" + "</body></h1>" +extra+ "</html> ";
  }

}